
drop table if exists personrolle; 

create table if not exists personrolle
(id int primary key auto_increment not null, rollenbezeichnung varchar, klasse int);

drop table if exists projektstatus;

create table if not exists projektstatus
(id int primary key auto_increment not null, name varchar);

drop table if exists projekt;

create table if not exists projekt
(id int primary key auto_increment not null, name varchar, beschreibung varchar(1000), ziele varchar, starttermin date(8), endtermin date(8), status int, kosten int, gebraucht int, deleted int default 0, foreign key (status) references projektstatus(id) ON DELETE CASCADE,
foreign key (status) references projektstatus(id));

drop table if exists person;

create table if not exists person
(id int primary key auto_increment not null, anrede varchar(10), name varchar(50), vorname varchar(50), telefonnummer varchar(13), email varchar, deleted int default 0);

drop table if exists projektbeteiligung;

create table if not exists projektbeteiligung
(id int primary key auto_increment not null, personid int, projektid int, rolleid int, pensum int, verfuegbarkeit varchar, interesse int, einfluss int,
deleted int default 0,
foreign key (personid) references person(id),
foreign key (projektid) references projekt(id) ON DELETE CASCADE,
foreign key (rolleid) references personrolle(id));

insert into personrolle (rollenbezeichnung, klasse) values ('Projektleiter', 1);
insert into personrolle (rollenbezeichnung, klasse) values ('Projektmitarbeiter', 1);
insert into personrolle (rollenbezeichnung, klasse) values ('Entwickler', 1);
insert into personrolle (rollenbezeichnung, klasse) values ('Geschäftsleitung', 2);
insert into personrolle (rollenbezeichnung, klasse) values ('Verwaltungsrat', 2);

insert into projektstatus (name) values ('aktiv');
insert into projektstatus (name) values ('archiviert');
insert into projektstatus (name) values ('ausserirdisch');

insert into projekt (name, beschreibung, ziele, starttermin, endtermin, status, kosten, gebraucht) values ('Neue Workstations', 'Alle Mitarbeiter der Abteilung "Buchhaltung" erhalten einen mobilen Arbeitsplatz', 'Die Arbeitsplätze müssen bis Ende April ausgeliefert sein', '2018-01-01', '2018-04-30', 1, 10000, 1340);
insert into projekt (name, beschreibung, ziele, starttermin, endtermin, status, kosten, gebraucht) values ('Leise Mausgeräte', 'Die Mäuse, die wir aktuell firmenweit benutzen, sind beim Klicken zu laut. In den Grossraumbüros haben wir bereits mehrere Reklamationen erhalten.', 'Geräuschreduktion um 50 % durch Klickgeräusche erreichen', '2018-05-04', '2019-04-30', 1, 500, 450);
insert into projekt (name, beschreibung, ziele, starttermin, endtermin, status, kosten, gebraucht) values ('Migration Exchange to Office365', 'Der Exchange Server (Hardware/Software) erreicht in 2 Jahren End of Life. Um für die Zukunft gerüstet zu sein, wird zu Office365 migriert', 'Mailverkehr ohne grossen Unterbruch (<6 Stunden), Exchange ersetzt vor End of Life', '2018-08-04', '2019-12-31', 3, 85000, 0);
insert into projekt (name, beschreibung, ziele, starttermin, endtermin, status, kosten, gebraucht) values ('Projektportfolioportal erstellen', 'Für das Projektportfoliomanagement wird eine Webapplikation realisiert, welche die Arbeiten für Projektportfoliomanager und Projektleiter vereinfacht', 'Projekte übersichtlich dargestellt, Ressourcen (Finanzen, Personal) pro Projekt ersichtlich', '2017-05-04', '2018-04-30', 2, 3000, 3200);

insert into person (anrede, name, vorname, telefonnummer, email, deleted) values ('Frau', 'Lüscher', 'Petra', '0772772727', 'name@mail.com', 0);
insert into person (anrede, name, vorname, telefonnummer, email, deleted) values ('Herr', 'Wyss', 'Emanuel', '0794664646', 'name@mail.com', 0);
insert into person (anrede, name, vorname, telefonnummer, email, deleted) values ('Herr', 'Willi', 'Michael', '0767214576', 'name@mail.com', 0);
insert into person (anrede, name, vorname, telefonnummer, email, deleted) values ('Frau', 'Gomez', 'Melanie', '0785707070', 'name@mail.com', 0);
insert into person (anrede, name, vorname, telefonnummer, email, deleted) values ('Herr', 'Rüdisühli', 'Theodor', '0794646464', 'name@mail.com', 0);

insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (1,2,1,40,'Mo-Di',30,40,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (2,2,2,60,'Mo-Mi',70,20,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (3,2,3,40,'Mo-Di',100,0,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (4,2,4,80,'Mo-Fr',30,30,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (5,2,5,100,'Mi-Fr',40,80,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (1,1,5,40,'Mo-Di',30,40,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (2,1,3,60,'Mo-Mi',70,20,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (3,1,4,40,'Mo-Di',100,0,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (4,1,2,80,'Mo-Fr',30,30,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (5,1,1,100,'Mi-Fr',40,80,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (1,3,5,40,'Mo-Di',30,40,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (2,3,2,60,'Mo-Mi',70,20,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (3,3,2,40,'Mo-Di',100,0,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (4,3,2,80,'Mo-Fr',30,30,0);
insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss,deleted) values (5,3,1,100,'Mi-Fr',40,80,0);

drop table if exists stakeholder;
drop table if exists projektmitarbeiter;
drop table if exists rolle; 