$( document ).ready(function() {
	$('select[name="state"]').change(function(){
		var url = window.location.href;
		if($(this).val() == 0){
			if(url.indexOf("?sort") !== -1) {
				if(url.indexOf("&state") !== -1) {
					window.open(url.substring(0, url.length - 8),"_self");
				} else {
					window.open(window.location.href,"_self");
				}
			} else {
				if(url.indexOf("?state") !== -1) {
					window.open(url.substring(0, url.length - 8),"_self");
				} else {
					window.open(window.location.href,"_self");
				}
			}
		} else {
			if(url.indexOf("?sort") !== -1) {
				if(url.indexOf("&state") !== -1) {
					window.open(url.substring(0, url.length - 8)+"&state="+$(this).val(),"_self");
				} else {
					window.open(window.location.href+"&state="+$(this).val(),"_self");
				}
			} else {
				if(url.indexOf("?state") !== -1) {
					window.open(url.substring(0, url.length - 8)+"?state="+$(this).val(),"_self");
				} else {
					window.open(window.location.href+"?state="+$(this).val(),"_self");
				}
			}
		}
	});
});