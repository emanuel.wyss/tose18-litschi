$( document ).ready(function() {
	var url = new URL(window.location.href);
	var action = url.searchParams.get("action");
	var msg = '';
	switch(action) {
	    case 'saved':
	        msg = "Erfolgreich gespeichert!"
	        break;
	    case 'created':
	        msg = "Erfolgreich erstellt!"
	        break;
	    case 'deleted':
	        msg = "Erfolgreich gelöscht!"
	        break;
	    case 'persdeleted':
	        msg = "Beteiligung erfolgreich gelöscht!"
	        break;
	    case 'perscreated':
	        msg = "Beteiligung erfolgreich erstellt!"
	        break;
	}
	if(msg.length >=1) {
		var bubble = '<div class="alert alert-primary" id="success-alert" >' +
		    '<button type="button" class="close" data-dismiss="alert">x</button>' +
		    msg +
		'</div>';
		$('.container').prepend(bubble);
	}
	
	$("#fehler").each(function(){
		if($(this).is(':empty')) {
			$(this).parent().remove();
		}
	});
	
});