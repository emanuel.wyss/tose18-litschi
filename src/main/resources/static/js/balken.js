$( document ).ready(function() {
	$('.balken').each(function(){
		var total = $(this).data('total');
		var used = $(this).data('used');
		var percentagereach = (100/total*used);
		var transp = (percentagereach + 1)
		if(percentagereach > 99) {
			$(this).css("background", "rgb(216, 87, 87)");
		} else if(percentagereach > 65) {
			$(this).css("background", "linear-gradient(to right, rgb(216, 87, 87) "+percentagereach+"%,#eff0f1 "+percentagereach+"%,rgba(0,0,0,0) "+transp+"%)");
		} else if(percentagereach > 32) {
			$(this).css("background", "linear-gradient(to right, rgb(214, 148, 16) "+percentagereach+"%,#eff0f1 "+percentagereach+"%,rgba(0,0,0,0) "+transp+"%)");
		} else {
			$(this).css("background", "linear-gradient(to right, #7DE560 "+percentagereach+"%,#eff0f1 "+percentagereach+"%,rgba(0,0,0,0) "+transp+"%)");
		}
	});
});