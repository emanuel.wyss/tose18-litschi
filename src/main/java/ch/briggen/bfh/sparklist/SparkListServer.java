package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.ProjectDeleteController;
import ch.briggen.bfh.sparklist.web.ProjectEditController;
import ch.briggen.bfh.sparklist.web.ProjectNewController;
import ch.briggen.bfh.sparklist.web.ProjectUpdateController;
import ch.briggen.bfh.sparklist.web.ProjectListManagementRootController;
import ch.briggen.bfh.sparklist.web.BeteiligungDeleteController;
import ch.briggen.bfh.sparklist.web.BeteiligungNewController;
import ch.briggen.bfh.sparklist.web.PersonDeleteController;
import ch.briggen.bfh.sparklist.web.PersonEditController;
import ch.briggen.bfh.sparklist.web.PersonNewController;
import ch.briggen.bfh.sparklist.web.PersonUpdateController;
import ch.briggen.bfh.sparklist.web.PersonListManagementRootController;

import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
		/**
		 * Kontroller für Projekt werden geholt oder gepostet, indem neue Objekte erstellt werden
		 */ 
		get("/", new ProjectListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/project", new ProjectEditController(), new UTF8ThymeleafTemplateEngine());
		post("/project/update", new ProjectUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/project/delete", new ProjectDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/project/new", new ProjectNewController(), new UTF8ThymeleafTemplateEngine());
		
		/**
		 * Kontroller für Personen werden geholt oder gepostet, indem neue Objekte erstellt werden
		 */ 
		get("/person", new PersonListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/person/edit", new PersonEditController(), new UTF8ThymeleafTemplateEngine());
		post("/person/update", new PersonUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/person/delete", new PersonDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/person/new", new PersonNewController(), new UTF8ThymeleafTemplateEngine());
		
		/**
		 * Kontroller für Projektbeteiligungen werden gepostet, indem neue Objekte erstellt werden
		 */ 
		// post("/beteiligung/update", new BeteiligungUpdateController(), new UTF8ThymeleafTemplateEngine());
		post("/beteiligung/new", new BeteiligungNewController(), new UTF8ThymeleafTemplateEngine());
		post("/beteiligung/delete", new BeteiligungDeleteController(), new UTF8ThymeleafTemplateEngine());
	}

}
