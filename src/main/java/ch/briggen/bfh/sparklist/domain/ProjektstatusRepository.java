package ch.briggen.bfh.sparklist.domain; 


import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;


import java.sql.Connection;

import java.sql.PreparedStatement;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.Collection;

import java.util.LinkedList;


import org.slf4j.Logger;

import org.slf4j.LoggerFactory;



/**
 	* Repository für alle Stati
	* Hier werden alle Funktionen für die DB-Operationen zu Stati implementiert
	* @author Emanuel Wyss
	**/



public class ProjektstatusRepository {

	private final Logger log = LoggerFactory.getLogger(ProjektstatusRepository.class);

	
		/**
		* Liefert alle Stati aus der Datenbank
		* @return Collection aller Stati
		*/
	public Collection<Projektstatus> getAll() {
	
		log.trace("getAll");
	
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name from projektstatus");
	
			ResultSet rs = stmt.executeQuery();
			return mapStatus(rs);
		}
		catch(SQLException e)
		{
				
			String msg = "SQL error while retreiving all Projektstati. "+e;
				
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	

	
	/**
	 * Helper zum konvertieren der Resultsets in Item-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Projektstatus> mapStatus(ResultSet rs) throws SQLException 
	{
		LinkedList<Projektstatus> list = new LinkedList<Projektstatus>();
		while(rs.next())
		{
			Projektstatus i = new Projektstatus(rs.getInt("id"),rs.getString("name"));
			list.add(i);
		}
		return list;
	}
	
	
}