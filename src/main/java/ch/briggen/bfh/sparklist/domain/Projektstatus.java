package ch.briggen.bfh.sparklist.domain; 

/**
 * Einzelner Eintrag in der Beiteiligtenliste mit einer eindeutigen Id
 * @author Melanie Gomez
 */

public class Projektstatus {
	
	private int id;
	private String name;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Projektstatus()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des Projektstatus
	 */
	
	public Projektstatus(int id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	/**
	 * Alle nachfolgenden get Funktionen geben die gewünschten Werte zurück
	 * @return gewünschter Wert
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Alle nachfolgenden set Funktionen setzen die gewünschten (bzw. eingegebenen Werte) ein
	 * @return none
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	/**kein toString notwendig
	public String toString() {
	}**/
	
}