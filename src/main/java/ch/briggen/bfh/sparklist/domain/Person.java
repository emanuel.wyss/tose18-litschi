package ch.briggen.bfh.sparklist.domain;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Email;
/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class Person {
	
	private int id;
	private String anrede;
	@NotNull
	private String name;
	@NotNull
	private String vorname;
	@NotNull @Size(min = 10, max = 13)
	private String telefonnummer;
	@NotNull @Email
	private String email;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Person()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param anrede  Anrede der Person (Frau / Mann)
	 * @param name Nachname der Person
	 * @param vorname Vorname der Person
	 * @param telefonnummer Telefonnummer der Person
	 * @param email E-Mail der Person
	 */
	public Person(int id, String anrede, String name, String vorname, String telefonnummer, String email)
	{
		this.id = id;
		this.anrede = anrede;
		this.name = name;
		this.vorname = vorname;
		this.telefonnummer = telefonnummer;
		this.email = email;
	}

	/**
	 * Alle nachfolgenden get Funktionen geben die gewünschten Werte zurück
	 * @return gewünschter Wert
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Alle nachfolgenden set Funktionen setzen die gewünschten (bzw. eingegebenen Werte) ein
	 * @return none
	 */
	public void setId (int id) {
		this.id = id;
	}
	

	public String getAnrede() {
		return anrede;
	}
	

	public void setAnrede(String anrede) {
		this.anrede = anrede;
	}
	

	public String getName() {
		return name;
	}
	

	public void setName(String name) {
		this.name = name;
	}
	
	public String getVorname() {
		return vorname;
	}
	
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * toString gibt alle Werte einer Person zurück: id, anrede, name, vorname, telefonnummer, email
	 * @return String mit allen Werten zu einer bestimmten Person
	 */
	public String toString() {
		return String.format("Person:{id: %d; anrede: %s; name: %s; vorname: %s; telefonnummer: %s; email: %s;}", id, anrede, name, vorname, telefonnummer, email);
	}
	
}
