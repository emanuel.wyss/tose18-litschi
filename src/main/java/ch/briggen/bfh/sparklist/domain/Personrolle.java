package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Beiteiligtenliste mit einer eindeutigen Id
 * @author Melanie Gomez
 */

public class Personrolle {
	
	private long id;
	private String rollenbezeichnung;
	private int klasse;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Personrolle()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param rollenbezeichnung Bezeichnung einer Rolle (Bsp: CEO)
	 * @param klasse Klassenunterschied zwischen Projektmitglied (1) und Stakeholder (2)
	 */
	public Personrolle(long id, String rollenbezeichnung, int klasse)
	{
		this.id = id;
		this.rollenbezeichnung = rollenbezeichnung;
		this.klasse = klasse;
	}
	
	/**
	 * Alle nachfolgenden get Funktionen geben die gewünschten Werte zurück
	 * @return gewünschter Wert
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Alle nachfolgenden set Funktionen setzen die gewünschten (bzw. eingegebenen Werte) ein
	 * @return none
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	public String getRollenbezeichnung() {
		return rollenbezeichnung;
	}
	
	public void setRollenbezeichnung(String rollenbezeichnung) {
		this.rollenbezeichnung = rollenbezeichnung;
	}

	public int getKlasse() {
		return klasse;
	}
	
	public void setKlasse(int klasse) {
		this.klasse = klasse;
	}
	
	/**
	 * toString gibt alle Werte einer Rolle zurück: id, rollenbezeichnung, klasse
	 * @return String mit allen Werten zu einer bestimmten Rolle
	 */
	public String toString() {
		return String.format("Personrolle:{id: %d; rollenbezeichnung: %s; klasse: %d;}", id, rollenbezeichnung, klasse);
	}
		
}