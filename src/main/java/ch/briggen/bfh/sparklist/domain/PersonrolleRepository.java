package ch.briggen.bfh.sparklist.domain; 


import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;


import java.sql.Connection;

import java.sql.PreparedStatement;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.Collection;

import java.util.LinkedList;


import org.slf4j.Logger;

import org.slf4j.LoggerFactory;



/**
 	* Repository für alle Stati.
 
	* Hier werden alle Funktionen für die DB-Operationen zu Stati implementiert
 
	* @author
 
	**/



public class PersonrolleRepository {

	private final Logger log = LoggerFactory.getLogger(PersonrolleRepository.class);

	
		/**
		* Liefert alle Rollen in der Datenbank
		* @return Collection aller Rollen
		*/
	public Collection<Personrolle> getAll() {
	
		log.trace("getAll");
	
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, rollenbezeichnung, klasse from personrolle");
	
			ResultSet rs = stmt.executeQuery();
			return mapRolle(rs);
		}
		catch(SQLException e)
		{
				
			String msg = "SQL error while retreiving all Personrollen. "+e;
				
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	
	/**
	* Liefert alle Rollen aus der Datenbank, die die Klasse "Projektmitglieder" (1) inne haben 
	* @return Collection aller Rollen mit Klasse = 1 (Projektmitglieder)
	*/
	public Collection<Personrolle> getAllMitarbeiterRollen() {
	
		log.trace("getAll");
	
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, rollenbezeichnung, klasse from personrolle where klasse = 1");
	
			ResultSet rs = stmt.executeQuery();
			return mapRolle(rs);
		}
		catch(SQLException e)
		{
				
			String msg = "SQL error while retreiving all Personrollen. "+e;
				
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	* Liefert alle Rollen aus der Datenbank, die die Klasse "Stakeholder" (2) inne haben 
	* @return Collection aller Rollen mit Klasse = 2 (Stakeholder)
	*/
	public Collection<Personrolle> getAllStakeholderRollen() {
	
		log.trace("getAll");
	
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, rollenbezeichnung, klasse from personrolle where klasse = 2");
	
			ResultSet rs = stmt.executeQuery();
			return mapRolle(rs);
		}
		catch(SQLException e)
		{
				
			String msg = "SQL error while retreiving all Personrollen. "+e;
				
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	

	
	/**
	 * Helper zum konvertieren der Resultsets in Item-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Personrolle> mapRolle(ResultSet rs) throws SQLException 
	{
		LinkedList<Personrolle> list = new LinkedList<Personrolle>();
		while(rs.next())
		{
			Personrolle i = new Personrolle(rs.getInt("id"),rs.getString("rollenbezeichnung"), rs.getInt("klasse"));
			list.add(i);
		}
		return list;
	}
	
	
}