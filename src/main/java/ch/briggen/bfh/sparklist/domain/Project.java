package ch.briggen.bfh.sparklist.domain;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.concurrent.TimeUnit;

/**
 * Einzelner Eintrag in der Projektliste mit einer eindeutigen Id
 * @author Emanuel Wyss
 *
 */
public class Project {
	
	private long id;
	@NotNull 
	private String name;
	@NotNull
	@Min(120)
	private String description;
	@NotNull 
	private String goals;
	@NotNull 
	private LocalDate startdate;
	@NotNull 
	private LocalDate enddate;
	@NotNull 
	private int state;
	@NotNull
	@Min(1)
	private int costs;
	@NotNull
	private int gebraucht;
	private long tage;
	private long resttage;

	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Project()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Projektname
	 * @param description Projektbeschreibung
	 * @param goals Projektziele
	 * @param startdate Startdatum des Projekts
	 * @param enddate Enddatum des Projekts
	 * @param state Status des Projekts 
	 * @param costs Projektkosten
	 * @param gebraucht gebrauchte Projektkosten
	 */
	public Project(long id, String name, String description, String goals, LocalDate startdate, LocalDate enddate, int state, int costs, int gebraucht)
	{
		this.id = id;
		this.name = name;
		this.description = description;
		this.goals = goals;
		this.startdate = startdate;
		this.enddate = enddate;
		this.state = state;
		this.costs = costs;
		this.gebraucht = gebraucht;
		this.tage = this.getTage();
		this.resttage = this.getResttage();
	}
	
	/**
	 * Alle nachfolgenden get Funktionen geben die gewünschten Werte zurück
	 * @return gewünschter Wert
	 */
	public String getName() {
		return name;
	}

	/**
	 * Alle nachfolgenden set Funktionen setzen die gewünschten (bzw. eingegebenen Werte) ein
	 * @return none
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGoals() {
		return goals;
	}

	public void setGoals(String goals) {
		this.goals = goals;
	}

	public void setStartdate(LocalDate startdate) {
		this.startdate = startdate;
	}

	public LocalDate getStartdate() {
		return startdate;
	}

	public void setEnddate(LocalDate enddate) {
		this.enddate = enddate;
	}

	public LocalDate getEnddate() {
		return enddate;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getState() {
		return state;
	}
	
	/**
	 * Alle nachfolgenden get Funktionen geben die gewünschten Werte zurück
	 * Initialisiert leeren statename und unterscheidet dann mit der switch Funktion zwischen den Cases 1/2/3
	 * @return String (Aktiv / Archiviert / Ausserirdisch)
	 */
	public String getStateName() {
		String statename = "";
		switch (state) {
	        case 1:  statename = "Aktiv";
	                 break;
	        case 2:  statename = "Archiviert";
            		break;
	        case 3:  statename = "Ausserirdisch";
            		break;
		}
		return statename;
	}

	public void setCosts(int costs) {
		this.costs = costs;
	}

	public int getCosts() {
		return costs;
	}

	public void setGebraucht(int gebraucht) {
		this.gebraucht = gebraucht;
	}

	public int getGebraucht() {
		return gebraucht;
	}

	public long getTage() {
		ZonedDateTime zdtend = enddate.atStartOfDay().atZone(ZoneId.of("Europe/Paris"));
		ZonedDateTime zdtstart = startdate.atStartOfDay().atZone(ZoneId.of("Europe/Paris"));
		long diff = zdtend.toInstant().toEpochMilli() - zdtstart.toInstant().toEpochMilli();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	public long getResttage() {
		ZonedDateTime zdtend = enddate.atStartOfDay().atZone(ZoneId.of("Europe/Paris"));
		long diff = zdtend.toInstant().toEpochMilli() - System.currentTimeMillis();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * toString gibt alle Werte einer Rolle zurück: id, name, description, goals, startdate, enddate, state, costs
	 * @return String mit allen Werten zu einem bestimmten Projekt
	 */
	public String toString() {
		return String.format("Item:{id: %d; name: %s; description: %s; goals: %s; startdate: %tF; enddate: %tF; state: %d; costs: %d; gebraucht: %d; tage: %d; resttage: %d}", id, name, description, goals, startdate, enddate, state, costs, gebraucht, tage, resttage);
	}
	

}
