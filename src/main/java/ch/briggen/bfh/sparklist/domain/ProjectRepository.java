package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.LinkedList;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Projects. 
 * Hier werden alle Funktionen für die DB-Operationen zu Projects implementiert
 * @author Marcel Briggen
 *
 */


public class ProjectRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjectRepository.class);
	

	/**
	 * Liefert alle Projekte aus der Datenbank
	 * @return Collection aller Projects
	 */
	public Collection<Project> getAll(String sort)  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, beschreibung, ziele, starttermin, endtermin, status, kosten, gebraucht from projekt where deleted = 0 order by "+sort+"");
			ResultSet rs = stmt.executeQuery();
			return mapProjects(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all projects. Error: "+e;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	/**
	 * Liefert alle Projekte aus der Datenbank mit dem angegebenen Status (state) aus
	 * @param state
	 * @return Collection aller Projects mit einem bestimmten Status (state)
	 */
	public Collection<Project> getByState(String state, String sort)  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, beschreibung, ziele, starttermin, endtermin, status, kosten, gebraucht from projekt where deleted = 0 and status = "+state+" order by "+sort+"");
			ResultSet rs = stmt.executeQuery();
			return mapProjects(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all projects. Error: "+e;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Projekte mit dem angegebenen Namen "name"
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Project> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, beschreibung, ziele, starttermin, endtermin, status, kosten, gebraucht from projekt where name=? and deleted = 0");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapProjects(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projects by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Projekt mit der übergebenen Id
	 * @param id id des Projekts
	 * @return Projekt oder NULL
	 */
	public Project getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, beschreibung, ziele, starttermin, endtermin, status, kosten, gebraucht from projekt where id=? and deleted = 0");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjects(rs).iterator().next();	
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projects by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Project in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(@Valid Project i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update projekt set name=?, beschreibung=?, ziele=?, starttermin=?, endtermin=?, status=?, kosten=?, gebraucht=? where id=?");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getDescription());
			stmt.setString(3, i.getGoals());	
			stmt.setDate(4, java.sql.Date.valueOf(i.getStartdate()));
			stmt.setDate(5, java.sql.Date.valueOf(i.getEnddate()));
			stmt.setInt(6, i.getState());
			stmt.setInt(7, i.getCosts());
			stmt.setInt(8, i.getGebraucht());
			stmt.setLong(9, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while saving project " + i + " / Error: " + e;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Projkt mit der angegebenen Id aus der Datenbank. DELETE.
	 * @param id Project ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update projekt set deleted = 1 where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing project by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Project in der DB. INSERT.
	 * @param i neu zu erstellendes Project
	 * @return Liefert die von der DB generierte id des neuen Projekts zurück
	 */
	public long insert(@Valid Project i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into projekt (name, beschreibung, ziele, starttermin, endtermin, status, kosten, gebraucht) values (?,?,?,?,?,?,?,?)");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getDescription());
			stmt.setString(3, i.getGoals());
			stmt.setDate(4, java.sql.Date.valueOf(i.getStartdate()));
			stmt.setDate(5, java.sql.Date.valueOf(i.getEnddate()));
			stmt.setInt(6, i.getState());
			stmt.setInt(7, i.getCosts());
			stmt.setInt(8, i.getGebraucht());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while inserting project " + i + " / Error: " + e;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Project-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Project> mapProjects(ResultSet rs) throws SQLException 
	{
		LinkedList<Project> list = new LinkedList<Project>();
		while(rs.next())
		{
			LocalDate starttermin = rs.getDate("starttermin").toLocalDate();
			LocalDate endtermin = rs.getDate("endtermin").toLocalDate();
			Project i = new Project(rs.getLong("id"),rs.getString("name"),rs.getString("beschreibung"),rs.getString("ziele"),starttermin, endtermin,rs.getInt("status"),rs.getInt("kosten"),rs.getInt("gebraucht"));
			list.add(i);
		}
		return list;
	}

}
