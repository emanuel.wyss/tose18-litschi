package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class Projektbeteiligung{
	
	private long id;
	private long personid;
	private long projektid;
	private long rolleid;
	private int pensum;
	private String verfuegbarkeit;
	private int interesse;
	private int einfluss;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Projektbeteiligung()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param personid ID der Person für die Projektbeteiligung
	 * @param projektid ID des Projekts für Projektbeteiligung
	 * @param rolleid ID der Rolle der Person, die sie im Projekt einnimmt
	 * @param pensum Pensum der Person im Projekt (wenn Rolle = 1 (Projektmitarbeiter))
	 * @param verfuegbarkeit Verfügbarkeit der Person im Projekt
	 * @param interesse Interesse der Person am Projekt (wenn Rolle = 2 (Stakeholder))
	 * @param einfluss Einfluss der Person am Projekt (wenn Rolle = 2 (Stakeholder))
	 */
	public Projektbeteiligung(long id, long personid, long projektid, long rolleid, int pensum, String verfuegbarkeit, int interesse, int einfluss)
	{
		this.id = id;
		this.personid = personid;
		this.projektid = projektid;
		this.rolleid = rolleid;
		this.pensum = pensum;
		this.verfuegbarkeit = verfuegbarkeit;
		this.interesse = interesse;
		this.einfluss = einfluss;
	}
	
	/**
	 * Alle nachfolgenden get Funktionen geben die gewünschten Werte zurück
	 * @return gewünschter Wert
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Alle nachfolgenden set Funktionen setzen die gewünschten (bzw. eingegebenen Werte) ein
	 * @return none
	 */
	public void setId (long id) {
		this.id = id;
	}
	
	public long getPersonid() {
		return this.personid;
	}
	
	public void setPersonid (long personid) {
		this.personid = personid;
	}
	
	public long getProjektid() {
		return  this.projektid;
	}
	
	public void setProjektid(long projektid) {
		this.projektid = projektid;
	}
	
	public long getRolleid() {
		return this.rolleid;
	}
	
	public void setRolleid(long rolleid) {
		this.rolleid = rolleid;
	}
	
	public int getPensum() {
		return pensum;
	}
	
	public void setPensum(int pensum) {
		this.pensum = pensum;
	}
	
	public String getVerfuegbarkeit() {
		return verfuegbarkeit;
	}

	public void setVerfuegbarkeit(String verfuegbarkeit) {
		this.verfuegbarkeit = verfuegbarkeit;
	}
	
	public int getInteresse() {
		return interesse;
	}
	
	public void setInteresse(int interesse) {
		this.interesse = interesse;
	}

	public int getEinfluss() {
		return einfluss;
	}

	public void setEinfluss(int einfluss) {
		this.einfluss = einfluss;
	}
	
	/**
	 * toString gibt alle Werte einer Projektbeteiligung zurück: id, personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss
	 * @return String mit allen Werten zu einer bestimmten Projektbeteiligung
	 */
	public String toString() {
		return String.format("Projektbeteiligung:{id: %d, personid: %d; projektid: %d; rolleid: %d; pensum: %s; verfuegbarkeit: %s; interesse: %d; einfluss: %d;}", id, personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss);
	}
	

}
