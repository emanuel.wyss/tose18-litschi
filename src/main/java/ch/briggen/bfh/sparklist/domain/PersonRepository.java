package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Items. 
 * Hier werden alle Funktionen für die DB-Operationen zu Items implementiert
 * @author Marcel Briggen
 *
 */


public class PersonRepository {
	
	private final static Logger log = LoggerFactory.getLogger(PersonRepository.class);
	

	/**
	 * Liefert alle items in der Datenbank
	 * @return Collection aller Personen
	 */
	public Collection<Person> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, anrede, name, vorname, telefonnummer, email from person where deleted = 0");
			ResultSet rs = stmt.executeQuery();
			return mapPersons(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all persons. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Personen mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Person> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, anrede, name, vorname, telefonnummer, email from personen where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapPersons(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving persons by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Item mit der übergebenen Id
	 * @param id id der Person
	 * @return Person oder NULL
	 */
	public Person getById(int id) {
		log.trace("getById " + id);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, anrede, name, vorname, telefonnummer, email from person where id=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapPersons(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving persons by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene item in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(@Valid Person i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update person set anrede=?, name=?, vorname=?, telefonnummer=?, email=? where id=?");
			stmt.setString(1, i.getAnrede());
			stmt.setString(2, i.getName());
			stmt.setString(3, i.getVorname());
			stmt.setString(4, i.getTelefonnummer());
			stmt.setString(5, i.getEmail());
			stmt.setInt(6, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Item mit der angegebenen Id von der DB
	 * @param id Person ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update person set deleted = 1 where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing persons by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Item in der DB. INSERT.
	 * @param i neu zu erstellende Person
	 * @return Liefert die von der DB generierte id der neuen Person zurück
	 */
	public long insert(@Valid Person i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into person (anrede, name, vorname, telefonnummer, email) values (?,?,?,?,?)");
			stmt.setString(1, i.getAnrede());
			stmt.setString(2, i.getName());
			stmt.setString(3, i.getVorname());
			stmt.setString(4, i.getTelefonnummer());
			stmt.setString(5, i.getEmail());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			int id = key.getInt(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while inserting person " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Project-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Person> mapPersons(ResultSet rs) throws SQLException 
	{
		LinkedList<Person> list = new LinkedList<Person>();
		while(rs.next())
		{
			Person i = new Person(rs.getInt("id"),rs.getString("anrede"),rs.getString("name"),rs.getString("vorname"),rs.getString("telefonnummer"),rs.getString("email"));
			list.add(i);
		}
		return list;
	}

}
