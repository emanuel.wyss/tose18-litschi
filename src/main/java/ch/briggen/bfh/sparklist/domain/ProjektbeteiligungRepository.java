package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Items. 
 * Hier werden alle Funktionen für die DB-Operationen zu Items implementiert
 * @author Marcel Briggen
 *
 */


public class ProjektbeteiligungRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjektbeteiligungRepository.class);
	

	/**
	 * Liefert alle Projektbeteiligung aus der Datenbank
	 * @return Collection aller Projektbeteiligungen
	 */
	public Collection<Projektbeteiligung> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss from projektbeteiligung");
			ResultSet rs = stmt.executeQuery();
			return mapProjektbeteiligung(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all projektbeteiligungen. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert die Projektbeteiligung mit der übergebenen id der Projektbeteiligung
	 * @param id der projektbeteiligung
	 * @return Projektbeteiligung oder NULL
	 */
	
	public Projektbeteiligung getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss from projektbeteiligung where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjektbeteiligung(rs).iterator().next();	
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projektbeteiligung by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert die Projektbeteiligung mit der übergebenen personid
	 * @param personid
	 * @return Projektbeteiligung oder NULL
	 */
	public Collection<Projektbeteiligung> getBypersonid(long personid) {
		log.trace("getBypersonid " + personid);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss from projektbeteiligung where personid=?");
			stmt.setLong(1, personid);
			ResultSet rs = stmt.executeQuery();
			return mapProjektbeteiligung(rs);			
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projektbeteiligung by id " + personid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	
	/**
	 * Liefert die Projektbeteiligung mit der übergebenen projektid
	 * @param projektid
	 * @return Projektbeteiligung oder NULL
	 */
	public Collection<Projektbeteiligung> getByProjektId(long projektid) {
		log.trace("getByprojektid " + projektid);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss from projektbeteiligung where projektid=?");
			stmt.setLong(1, projektid);
			ResultSet rs = stmt.executeQuery();
			return mapProjektbeteiligung(rs);	
			//Collection<Projektbeteiligung> test = Collections.emptySet();
			//return test;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving projektbeteiligung by projektid " + projektid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Speichert die Änderungen der übergebene Projektbeteiligung in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Projektbeteiligung i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update projektbeteiligung set personid=?, projektid=?, rolleid=?, pensum=?, verfuegbarkeit=?, interesse=?, einfluss=? where personid=?");
			stmt.setLong(1, i.getPersonid());
			stmt.setLong(2, i.getProjektid());
			stmt.setLong(3, i.getRolleid());
			stmt.setInt(4, i.getPensum());
			stmt.setString(5, i.getVerfuegbarkeit());
			stmt.setInt(6, i.getInteresse());
			stmt.setInt(7, i.getEinfluss());
			stmt.setLong(8, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht die Projektbeteiligung mit der angegebenen Id aus der DB
	 * @param id der projektbeteiligung
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from projektbeteiligung where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing projektbeteiligung by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert die neu erfasste Projektbeteiligung in der DB. INSERT.
	 * @param i neu zu erstellende Projektbeteiligung
	 * @return Liefert die von der DB generierte id der neuen Projektbeteiligung zurück
	 */
	public long insert(Projektbeteiligung i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into projektbeteiligung (personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss) values (?,?,?,?,?,?,?)");
			stmt.setLong(1, i.getPersonid());
			stmt.setLong(2, i.getProjektid());
			stmt.setLong(3, i.getRolleid());
			stmt.setInt(4, i.getPensum());
			stmt.setString(5, i.getVerfuegbarkeit());
			stmt.setInt(6, i.getInteresse());
			stmt.setInt(7, i.getEinfluss());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating projektbeteiligung " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Item-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<Projektbeteiligung> mapProjektbeteiligung(ResultSet rs) throws SQLException 
	{
		LinkedList<Projektbeteiligung> list = new LinkedList<Projektbeteiligung>();
		while(rs.next())
		{
			// personid, projektid, rolleid, pensum, verfuegbarkeit, interesse, einfluss
			Projektbeteiligung i = new Projektbeteiligung(rs.getLong("id"), rs.getLong("personid"), rs.getLong("projektid"), rs.getLong("rolleid"), rs.getInt("pensum"), rs.getString("verfuegbarkeit"), rs.getInt("interesse"), rs.getInt("einfluss"));
			list.add(i);
		}
		return list;
	}

}
