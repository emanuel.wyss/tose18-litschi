package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjektbeteiligungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Projects
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class BeteiligungDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(BeteiligungDeleteController.class);
		
	private ProjektbeteiligungRepository beteiligungRepo = new ProjektbeteiligungRepository();
	

	/**
	 * Löscht die Projektbeteiligung mit der übergebenen id in der Datenbank
	 * /beteiligung/delete&id=987 löscht die Projektbeteiligung mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf POST /beteiligung/delete
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("projektBeteiligung.id");
		log.trace("POST /beteiligung/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		beteiligungRepo.delete(longId);
		response.redirect("/project?id="+request.queryParams("projektBeteiligung.projectid")+"&action=persdeleted");
		return null;
	}
}


