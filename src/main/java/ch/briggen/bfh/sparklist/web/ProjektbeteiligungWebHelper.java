package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Projektbeteiligung;
import spark.Request;

class ProjektbeteiligungWebHelper {
	@SuppressWarnings("unused") 
	private final static Logger log = LoggerFactory.getLogger(ProjektbeteiligungWebHelper.class);
	
	public static Projektbeteiligung beteiligungFromWeb(Request request)
	{
		return new Projektbeteiligung(
				Long.parseLong(request.queryParams("projektBeteiligung.id")),
				Long.parseLong(request.queryParams("projektBeteiligung.personid")),
				Long.parseLong(request.queryParams("projektBeteiligung.projektid")),
				Long.parseLong(request.queryParams("projektBeteiligung.rolleid")),
				Integer.parseInt(request.queryParams("projektBeteiligung.pensum")),
				request.queryParams("projektBeteiligung.verfuegbarkeit"),
				Integer.parseInt(request.queryParams("projektBeteiligung.interesse")),
				Integer.parseInt(request.queryParams("projektBeteiligung.einfluss")));
	}

}
