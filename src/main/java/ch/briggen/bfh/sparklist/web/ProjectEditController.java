package ch.briggen.bfh.sparklist.web; 

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.*;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Projekte
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ProjectEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ProjectEditController.class);
	
	

	private ProjectRepository projectRepo = new ProjectRepository();
	private ProjektstatusRepository statusRepo = new ProjektstatusRepository();
	private PersonRepository personRepo = new PersonRepository();
	private ProjektbeteiligungRepository beteiligungRepo = new ProjektbeteiligungRepository();
	private PersonrolleRepository personrolleRepo = new PersonrolleRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Projekts 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Project erstellt (Aufruf von /project/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Project mit der übergebenen id upgedated (Aufruf /project/save)
	 * Hört auf GET /project
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "projectDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		Map<String, Object> model = new HashMap<String, Object>();
		
		String fm = request.queryParams("error");
		if( null != fm) {
			model.put("fehler", fm);
		}
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /project für INSERT mit id " + idString);
			//der Submit-Button ruft /project/new auf --> INSERT
			model.put("postAction", "/project/new");
			model.put("projectDetail", new Project());
			model.put("statuss", statusRepo.getAll());
			model.put("persons", personRepo.getAll());

		}
		else
		{
			log.trace("GET /project für UPDATE mit id " + idString);
			//der Submit-Button ruft /project/update auf --> UPDATE
			model.put("postAction", "/project/update");
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "projectDetail" hinzugefügt. projectDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Project i = projectRepo.getById(id);
			model.put("projectDetail", i);
			model.put("statuss", statusRepo.getAll());
			model.put("persons", personRepo.getAll());
			model.put("stakeholders", personrolleRepo.getAllStakeholderRollen());
			model.put("mitarbeiters", personrolleRepo.getAllMitarbeiterRollen());
			model.put("beteiligungs", beteiligungRepo.getByProjektId(id));
			model.put("deleteAction", "/beteiligung/delete");
			model.put("addAction", "/beteiligung/new");
			
		}
		
		//das Template projectDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "projectDetailTemplate");
	}
	
	
	
}


