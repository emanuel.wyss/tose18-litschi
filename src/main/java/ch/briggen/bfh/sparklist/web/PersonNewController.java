package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Items
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class PersonNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(PersonNewController.class);
		
	private PersonRepository personRepo = new PersonRepository();
	
	/**
	 * Erstellt eine neue Person in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /person&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /person/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Person personDetail = PersonWebHelper.personFromWeb(request);
		log.trace("POST /person/new mit personDetail " + personDetail);
		
		String fehler;
		
		if(personDetail.getName().isEmpty()) {
			fehler = "Name darf nicht leer sein";
			response.redirect("/person/edit?error="+fehler);
		} else if(personDetail.getVorname().isEmpty()) {
			fehler = "Vorname darf nicht leer sein";
			response.redirect("/person/edit?error="+fehler);
		} else if(personDetail.getVorname().isEmpty()) {
			fehler = "Vorname darf nicht leer sein";
			response.redirect("/person/edit?error="+fehler);
		} else if(personDetail.getAnrede().isEmpty()) {
			fehler = "Anrede darf nicht leer sein";
			response.redirect("/person/edit?error="+fehler);
		} else if(personDetail.getEmail().isEmpty()) {
			fehler = "Email darf nicht leer sein";
			response.redirect("/person/edit?error="+fehler);
		} else {
			//insert gibt die von der DB erstellte id zurück.
			long id = personRepo.insert(personDetail);
			
			//die neue Id wird dem Redirect als Parameter hinzugefügt
			//der redirect erfolgt dann auf /project?id=432932
			if(request.queryParamOrDefault("personDetail.saveAndNew", null)!=null) {
				response.redirect("/person/edit");
			} else {
				response.redirect("/person/edit?id="+id+"&action=created");
			}	
		}
		
		return null;
	}
}


