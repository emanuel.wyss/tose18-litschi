package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import ch.briggen.bfh.sparklist.domain.ProjektstatusRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Projektliste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author M. Briggen
 *
 */
public class ProjectListManagementRootController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ProjectListManagementRootController.class);

	ProjectRepository repository = new ProjectRepository();
	ProjektstatusRepository statusRepo = new ProjektstatusRepository();

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Projects werden geladen und die Collection dann für das Template unter dem namen "project" bereitgestellt
		//Das Template muss dann auch den Namen "project" verwenden.
		HashMap<String, Object> model = new HashMap<String, Object>();
		String sort = "id";
		if(request.queryParams("sort") != null && !request.queryParams("sort").isEmpty()) sort = request.queryParams("sort");
		if(request.queryParams("state") != null && !request.queryParams("state").isEmpty()) {
			model.put("projects", repository.getByState(request.queryParams("state"), sort));
			model.put("selstatus", request.queryParams("state"));
		} else {
			model.put("projects", repository.getAll(sort));
			model.put("selstatus", 0);
		}
		model.put("statuss", statusRepo.getAll());
		return new ModelAndView(model, "projectTemplate");
	}
}
