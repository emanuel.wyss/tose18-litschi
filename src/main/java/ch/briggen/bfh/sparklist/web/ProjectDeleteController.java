package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Projekten
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ProjectDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectDeleteController.class);
		
	private ProjectRepository projectRepo = new ProjectRepository();
	

	/**
	 * Löscht das Projekt mit der übergebenen id in der Datenbank
	 * /project/delete?id=987 löscht das Project mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /project/delete
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /project/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		projectRepo.delete(longId);
		response.redirect("/?action=deleted");
		return null;
	}
}


