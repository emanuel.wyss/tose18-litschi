package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Project;
import ch.briggen.bfh.sparklist.domain.ProjectRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Projekten
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class ProjectNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjectNewController.class);
		
	private ProjectRepository projectRepo = new ProjectRepository();
	
	/**
	 * Erstellt ein neues Projekt in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /project&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /project/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Project projectDetail = ProjectWebHelper.projectFromWeb(request);
		log.trace("POST /project/new mit projectDetail " + projectDetail);
		
		String fehler;
		
		if(projectDetail.getName().isEmpty()) {
			fehler = "Name darf nicht leer sein";
			response.redirect("/project?error="+fehler);
		} else if(projectDetail.getDescription().isEmpty()) {
			fehler = "Beschreibung darf nicht leer sein";
			response.redirect("/project?error="+fehler);
		//} else if(projectDetail.getStartdate().isBefore(projectDetail.getEnddate())) {
			//fehler = "Startdatum muss vor Enddatum sein";
			//response.redirect("/project?error="+fehler);
		//} else if( 0 < projectDetail.getState() && projectDetail.getState() < 5) {
			//fehler = "ungültiger Status";
			//response.redirect("/project?error="+fehler);
		} else if(projectDetail.getCosts() < 0) {
			fehler = "Kosten dürfen nicht kleiner als 0 sein.";
			response.redirect("/project?error="+fehler);
		} else {
			
			//insert gibt die von der DB erstellte id zurück.
			long id = projectRepo.insert(projectDetail);
		
			//die neue Id wird dem Redirect als Parameter hinzugefügt
			//der redirect erfolgt dann auf /project?id=432932
			if(request.queryParamOrDefault("projectDetail.saveAndNew", null)!=null) {
				response.redirect("/project");
			} else {
				response.redirect("/project?id="+id+"&action=created");
			}
			
		}
		return null;
	}
}


