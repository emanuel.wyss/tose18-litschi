package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Personen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!! 
 * @author Marcel Briggen
 *
 */

public class PersonDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(PersonDeleteController.class);
		
	private PersonRepository personRepo = new PersonRepository();
	

	/**
	 * Löscht die Person mit der übergebenen id aus der Datenbank
	 * /person/delete&id=987 löscht die Person mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /person/delete
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /person/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		personRepo.delete(longId);
		response.redirect("/person?action=deleted");
		return null;
	}
}


