package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Projektbeteiligung;
import ch.briggen.bfh.sparklist.domain.ProjektbeteiligungRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Projektbeteiligungen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class BeteiligungNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(BeteiligungNewController.class);
		
	private ProjektbeteiligungRepository beteiligungRepo = new ProjektbeteiligungRepository();
	

	/**
	 * Erstellt die Projektbeteiligung mit der übergebenen id in der Datenbank
	 * 
	 * Hört auf POST /beteiligung/new
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Projektbeteiligung projektBeteiligung = ProjektbeteiligungWebHelper.beteiligungFromWeb(request);
		log.trace("POST /beteiligung/new mit projektBeteiligung " + projektBeteiligung);
		
		//insert gibt die von der DB erstellte id zurück.
		beteiligungRepo.insert(projektBeteiligung);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /project?id=432932
		response.redirect("/project?id="+request.queryParams("projektBeteiligung.projektid")+"&action=perscreated");
		return null;
	}
}


