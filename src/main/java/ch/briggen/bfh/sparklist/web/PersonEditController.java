package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Projects
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class PersonEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(PersonEditController.class);
	
	
	
	private PersonRepository personRepo = new PersonRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Person
	 * Liefert das Formular (bzw. Template) zum Bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars eine neue Person erstellt (Aufruf von /person/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars die Person mit der übergebenen id upgedated (Aufruf /person/save)
	 * Hört auf GET /person
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "personDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		String fm = request.queryParams("error");
		if( null != fm) {
			model.put("fehler", fm);
		}
		
		
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /project für INSERT mit id " + idString);
			//der Submit-Button ruft /project/new auf --> INSERT
			model.put("postAction", "/person/new");
			model.put("personDetail", new Person());

		}
		else
		{
			log.trace("GET /person für UPDATE mit id " + idString);
			//der Submit-Button ruft /project/update auf --> UPDATE
			model.put("postAction", "/person/update");
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "projectDetail" hinzugefügt. projectDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			int id = Integer.parseInt(idString);
			Person i = personRepo.getById(id);
			model.put("personDetail", i);
		}
		
		//das Template projectDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "personDetailTemplate");
	}
	
	
	
}


