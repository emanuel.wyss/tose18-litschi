package ch.briggen.bfh.sparklist.web; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Projects
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class PersonUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(PersonUpdateController.class);
		
	private PersonRepository personRepo = new PersonRepository();
	


	/**
	 * Schreibt die Änderungen der Person zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/person) mit der Person-ID als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /person/update
	 * 
	 * @return redirect nach /project: via Browser wird /project aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Person personDetail = PersonWebHelper.personFromWeb(request);
		
		log.trace("POST /person/update mit personDetail " + personDetail);
		
		String fehler;
		
		if(personDetail.getName().isEmpty()) {
			fehler = "Name darf nicht leer sein";
			response.redirect("/person/edit?error="+fehler);
		} else if(personDetail.getVorname().isEmpty()) {
			fehler = "Vorname darf nicht leer sein";
			response.redirect("/person/edit?error="+fehler);
		} else if(personDetail.getVorname().isEmpty()) {
			fehler = "Vorname darf nicht leer sein";
			response.redirect("/person/edit?error="+fehler);
		} else if(personDetail.getAnrede().isEmpty()) {
			fehler = "Anrede darf nicht leer sein";
			response.redirect("/person/edit?error="+fehler);
		} else if(personDetail.getEmail().isEmpty()) {
			fehler = "Email darf nicht leer sein";
			response.redirect("/person/edit?error="+fehler);
		} else {
		
			//Speichern des Items in dann den Parameter für den Redirect abfüllen
			//der Redirect erfolgt dann z.B. auf /project&id=3 (wenn itemDetail.getId == 3 war)
			personRepo.save(personDetail); 
			if(request.queryParamOrDefault("personDetail.saveAndNew", null)!=null) {
				response.redirect("/person/edit");
			} else {
				response.redirect("/person/edit?id="+personDetail.getId()+"&action=saved");
			}
			
		}
		return null;
	}
}


