package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Personenliste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author M. Briggen
 *
 */
public class PersonListManagementRootController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(PersonListManagementRootController.class);

	PersonRepository repository = new PersonRepository();

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Projects werden geladen und die Collection dann für das Template unter dem namen "project" bereitgestellt
		//Das Template muss dann auch den Namen "project" verwenden.
		HashMap<String, Collection<Person>> model = new HashMap<String, Collection<Person>>();
		model.put("persons", repository.getAll());
		return new ModelAndView(model, "personTemplate");
	}
}
